/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mat.lib.jmsa;

import java.math.BigInteger;

/**
 *
 * @author Profe
 */
public class MatLib 
{



    private static double precision=0.0001;
    private static int TaylorSteps=20;
    private static double e_=2.718281828459045;

    /**
     *
     * @return
     */
    public static double getPrecision()
    {
        return MatLib.precision;
    }

    /**
     *
     * @param precision
     */
    public static void setPrecision(double precision)
    {
        MatLib.precision=precision;
    }

    /**
     *
     * @return
     */
    public static double getTaylorSteps()
    {
        return MatLib.TaylorSteps;
    }

    /**
     *
     * @param steps
     */
    public static void setTaylorSteps(int steps)
    {
        MatLib.TaylorSteps=steps;
    }    
    /**
     * @return the e_
     */
    public static double getE_()
    {
        return e_;
    }

    /**
     * @param aE_ the e_ to set
     */
    public static void setE_(double aE_)
    {
        e_ = aE_;
    }    
    /**
     *
     * @param b primer número a multiplicar
     * @param c segundo número a multiplicar
     * @return resultado de multiplicar b x c
     */
    public static long mult(long  b, long c)
    {
        if (b==0 || c==0)
            return 0;
        
        long signo_b = (b>=0) ? 1:-1;
        long signo_c = (c>=0) ? 1:-1;
        b = (b<0) ? (-b):b;
        c = (c<0) ? (-c):c;
        long contador = c;
        
        long Ac=0;
        contador --;
        while (contador >=0 )
        {
            Ac+=b;
            contador--;
        }
        return Ac*signo_b*signo_c;
    }
//-------------------------------
// X^N con X y N flotantes    
 // función auxiliar para x^n, x y n reales en representación double  

    /**
     *
     * @param x
     * @return
     */
    public static double square(double x)
    {
        return x * x;
    }
// meaning of 'precision': the returned answer should be base^x, where
//                         x is in [power-precision/2,power+precision/2]

    /**
     *
     * @param base
     * @param power
     * @param precision
     * @return
     */
    public static double mypow(double base, double power, double precision)
    {
        if (power < 0)
        {
            return 1 / mypow(base, -power, precision);
        }
        if (power >= 10)
        {
            return square(mypow(base, power / 2, precision / 2));
        }
        if (power >= 1)
        {
            return base * mypow(base, power - 1, precision);
        }
        if (precision >= 1)
        {
            return raizBabilon(base);
        }
        double tmp=mypow(base, power * 2, precision * 2);
        return raizBabilon(tmp);
    }

    /**
     *
     * @param base
     * @param power
     * @return
     */
    public static double mypow(double base, double power)
    {
        return mypow(base, power, .000001);
    }
//--------------------------------------------    
    //**diversos métodos para hacer la exponenciación de X elevado a N, N positivo y entero
    //** métodos para enteros exponenciación por elevacilón al cuadrado

    /**
     *
     * @param x
     * @param n
     * @return
     */
    public static long exp_by_squareBinary (long x, long n)
    {
        long pot=1;
        long signo = (x>=0) ? 1:-1;
        x=x*signo; //valor absoluto
        while(n!=0)
        {
            boolean zeroQ = ((n>>1) & 1) != 0; //comprueba que el bit más a la izquierda sea o no sea cero
          //  boolean zeroQ2=BigInteger.valueOf(n).testBit(1);
            if(!zeroQ)
            {
               pot *= x;
               n -= 1;
            }
            x *= x;
            n /= 2;
        }
        return pot*signo;
    }
 //************************

 //************************ 

    /**
     *
     * @param x
     * @param n
     * @return
     */
  
    public static double exp_by_squaring(double x, long n)
    {
    if (n < 0)
        return exp_by_squaring(1 / x, -n);
    else if (n == 0)  
        return  1;
    else if (n == 1)
        return  x ;
    else if (n%2==0) 
        return exp_by_squaring(x * x,  n / 2);
    return x * exp_by_squaring(x * x, (n - 1) / 2);
    }
//*************************************

    /**
     *
     * @param x
     * @param n
     * @return
     */
    public static double exp_by_squaring2(double x, long n)
    {
        return exp_by_squaring_tr(1, x, n);
    }
    
    /**
     *
     * @param y
     * @param x
     * @param n
     * @return
     */
    public static double exp_by_squaring_tr(double y, double x, long n)
    {
        if (n < 0)
            return exp_by_squaring_tr(y, 1 / x, - n);
        else if (n == 0) 
            return  y;
        else if (n == 1) 
            return  x * y;
        else if (n%2==0)
            return exp_by_squaring_tr(y, x * x,  n / 2);
        return exp_by_squaring_tr(x * y, x * x, (n - 1) / 2);
    }
            
//*************************************  
    
//*************************************    
    /**
     *
     * @param x
     * @param n
     * @return
     */
    public static double exp_by_squaring_iterative(double x, long n)
    {
        long signo = (x>=0) ? 1:-1;
        x=x*signo; //valor absoluto
        if (n < 0)
        {
            x = 1 / x;
            n = -n;
        }
        if (n == 0) return 1;
        double y = 1.0;
        while (n > 1)
        {
            if (n%2==0) //n is even then 
            {
                x = x * x;
                n = n / 2;                
            }
            else
            {
                y = x * y;
                x = x * x;
                n = (n - 1) / 2;                
            }
        }        
    return x * y*signo;
    }
    //Exponenciación Serie de taylor reescrita
    // Function returns approximate value of e^x  
    // using sum of first n terms of Taylor Series 
    // e^x = 1 + x/1! + x^2/2! + x^3/3! + ......
    // rewriten as e^x = 1 + (x/1) (1 + (x/2) (1 + (x/3) (........) ) )

    /**
     *
     * @param n
     * @param x
     * @return
     */
    public static double exponential(int n, double x) 
    { 
        // initialize sum of series 
        double sum = 1;  
   
        for (int i = n - 1; i > 0; --i ) 
            sum = 1 + x * sum / i; 
   
        return sum; 
    }
    //*************************************************
    // Function returns approximate value of e^x  
    // using sum of first n terms of Taylor Series 

    /**
     *
     * @param x
     * @return
     */
    public static double exponential(double x) 
    { 
        // initialize sum of series 
        double sum = MatLib.exponential(MatLib.TaylorSteps, x);
        return sum; 
    }
    //*************************************************
    // x^n=e^(nlogx)
    //exp by multiplication

    /**
     *
     * @param bas
     * @param exp
     * @return
     */
    public static double exponetiation_l(double bas,double exp)
    {
        double solution=0;
        solution=MatLib.exponential(exp*MatLib.logHyper(bas));
     return solution;
    }
    
    //*************************************************
    //**diversos métodos para calcular logaritmos*//

    /**
     *
     * @param x
     * @param steps
     * @return
     */
    public static double aproxLogHyper(double x,  int steps)
    {
        int k=1; // contador de pasos 
        double termino=(x-1)/(x+1);
        double apxlog=termino;
        for(int i=0;i<steps;i++)
        {
            k+=2;
            apxlog=apxlog+ (1.0/k)*exp_by_squaring_iterative(termino,k);
        }    
        return 2*apxlog;
    }

    /**
     *
     * @param x
     * @return
     */
    public static double logHyper(double x)
    {
        double log;
        if(x<=0)
            return Double.NaN;
        double y=aproxLogHyper(x, 3);
        double A=x/exponential(y);
        //double tA=((A-1)/(A+1));
        double lnA=aproxLogHyper(A, 3);
        log=y+lnA;
        return log;
    }
    /** diversos métodos para calcular la raíz cuadrada (aproximaciones)**/
    /**
     *
     * @param x número al que queremos calcular la raíz cuadrada por el método
     * babilónico - egipcio
     * @return raíz cuadrada de x
     */
    public static double raizBabilon(double x)
    {
      double r = x;
      double t = 0;
      while (t!=r)
      {
          t = r;
          r = 0.5 * ( (x/r) + r);
      }
      return r;
    }
    
    /**
     *
     * @param x
     * @return
     */
    public static double raizM2(double x)
    {
        long xi= Double.doubleToLongBits(x);
        return x;
    }

    /**
     *
     * @param x
     * @return
     */
    public static double raizBakhShali(double x)
    {
        double rs=x/2;
        
        return rs;
    }
}