/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mat.lib.jmsa;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author Profe
 */
public class MatLibNGTest
{
    public MatLibNGTest()
    {
    }

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception
    {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception
    {
    }

    @DataProvider(name = "test1")
    public static Object[][] primeNumbers()
    {
        return new Object[][]
        {{4,2,8},{2,4,8}, {3,7,21},{11,11,121},{23,13,299},
         {-4,2,-8},{2,-4,-8}, {3,-7,-21},{-11,11,-121},{-23,13,-299},
         {-4,-2,8},{-2,-4,8}, {-3,-7,21},{-11,-11,121},{-23,-13,299}};
    }

    /**
     * Test of mult method, of class MatLib.
     * @param b
     * @param c
     * @param expResult
     */
    @Test(dataProvider = "test1")
    public void testMult(long b,long c, long expResult)
    {
        System.out.println("mult");
        long result = MatLib.mult(b, c);
        assertEquals(result, expResult);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

}
